package Negocio;

import Datos.Jugador;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Lectura {
    private BufferedReader lector;

    public void abrir() {
        try {
            lector = new BufferedReader(new FileReader("jugadores.txt"));
        } catch (FileNotFoundException fileNotFoundException) {
            System.err.println("Error al encontrar el archivo");
        }
    }

    public List<Jugador> leer() {
        List<Jugador> listaJugadores = new ArrayList<>();

        try {
            Jugador jugador;
            String linea = lector.readLine();
            String[] datos;

            while(linea != null){
                jugador = new Jugador();
                datos = linea.split(";");
                jugador.setNombreUsuario(datos[0]);
                jugador.setNombre(datos[1]);
                jugador.setApellido(datos[2]);
                jugador.setEdad(Integer.parseInt(datos[3]));
                jugador.setPais(datos[4]);

                listaJugadores.add(jugador);
                linea = lector.readLine();
            }
        } catch (IOException ioException) {
            System.err.println("Error al leer el archivo");
        }

        return listaJugadores;
    }

    public void cerrar() {
        try {
            if (lector != null) {
                lector.close();
            }
        } catch (IOException ioException) {
            System.err.println("Error al cerrar el archivo");
        }
    }
}
