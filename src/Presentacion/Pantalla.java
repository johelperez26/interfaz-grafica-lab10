package Presentacion;

import javax.swing.*;
import java.io.File;

public class Pantalla {
    private JPanel panel;
    private JButton btnPrueba;

    public Pantalla() {
        JFrame frame = new JFrame("Pantalla");
        frame.setContentPane(panel);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(900, 500);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);

        File archivo = new File("llama.jpg");
        JLabel imagen = new JLabel(new ImageIcon(archivo.getName()));

        File archivo2 = new File("shulk.jpg");
        JLabel imagen2 = new JLabel(new ImageIcon(archivo2.getName()));

        imagen.setBounds(20, 20, 450, 450);
        imagen2.setBounds(500, 20, 300, 300);
        btnPrueba.setBounds(150, 150, 100, 50);

        panel.add(imagen);
        panel.add(imagen2);

        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new Pantalla();
    }
}
